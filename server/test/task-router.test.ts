import * as http from 'http';
import axios from 'axios';
import pool from '../src/mysql-pool';
import taskService, { Task } from '../src/task-service';
import app from '../src/app';

axios.defaults.baseURL = 'http://localhost:3001/api/v1';

const testTasks: Task[] = [
  { id: 1, title: 'Les leksjon', done: false },
  { id: 2, title: 'Møt opp på forelesning', done: false },
  { id: 3, title: 'Gjør øving', done: false },
];

let webServer: http.Server | null;
beforeAll((done) => {
  // Use separate port for testing
  webServer = app.listen(3001, () => done());
});

beforeEach((done) => {
  // Delete all tasks, and reset id auto-increment start value
  pool.query('TRUNCATE TABLE Tasks', async (error) => {
    if (error) return done(error);

    for (const task of testTasks) await taskService.create(task.title);
    done();
  });
});

afterAll((done) => {
  if (!webServer) return done(new Error());
  webServer.close(() => pool.end(() => done()));
});

describe('Fetch tasks (GET)', () => {
  test('Fetch all tasks (200 OK)', async () => {
    const response = await axios.get<Task[]>('/tasks');
    expect(response.status).toEqual(200);
    expect(response.data).toEqual(testTasks);
  });

  test('Fetch task (200 OK)', async () => {
    const response = await axios.get<Task>('/tasks/1');
    expect(response.status).toEqual(200);
    expect(response.data).toEqual(testTasks[0]);
  });

  test('Fetch task (404 Not Found)', async () => {
    try {
      await axios.get<Task>('/tasks/4'); // Should throw
      expect(true).toEqual(false);
    } catch (error) {
      if (error instanceof Error)
        expect(error.message).toEqual('Request failed with status code 404');
      else expect(true).toEqual(false);
    }
  });
});
