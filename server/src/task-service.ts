import pool from './mysql-pool';
import type { RowDataPacket, ResultSetHeader } from 'mysql2';

const promisePool = pool.promise();

// The "interface" keyword could be used here instead of "Task =", although types are a bit more flexible
export type Task = {
  id: number;
  title: string;
  done: boolean;
};

class TaskService {
  /**
   * Get all tasks.
   */
  async getAll(): Promise<Task[]> {
    const [rows]: [RowDataPacket] = await promisePool.query('SELECT * FROM Tasks');
    return rows as Task[];
  }

  /**
   * Get task with given id.
   */
  async get(id: number): Promise<Task | undefined> {
    const [rows]: [RowDataPacket] = await promisePool.query('SELECT * FROM Tasks WHERE id=?', [id]);
    return rows[0];
  }

  /**
   * Create new task having the given title.
   *
   * Resolves the newly created task id.
   */
  async create(title: string): Promise<number> {
    const [rows]: [ResultSetHeader] = await promisePool.query('INSERT INTO Tasks SET title=?', [
      title,
    ]);
    return rows.insertId;
  }
}

export default new TaskService();
