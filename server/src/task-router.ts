import express from 'express';
import taskService from './task-service';

/**
 * Express router containing task methods.
 */
const router = express.Router();

router.get('/tasks', async (_request, response) => {
  try {
    const tasks = await taskService.getAll();
    response.send(tasks);
  } catch (error) {
    response.status(500).send(error);
  }
});

router.get('/tasks/:id', async (request, response) => {
  try {
    const task = await taskService.get(Number(request.params.id));
    if (!task) return response.status(404).send('Task not found');
    response.send(task);
  } catch (error) {
    response.status(500).send(error);
  }
});

export default router;
