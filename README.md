# Example TODO web application with backend only

This project demonstrates an initial backend of a TODO application that can be used to manage tasks
that need to be completed.

# Prerequisites

- Node (and npm)
- MySQL running on your local machine
- Linux or MacOS (Windows could work, but you might need to set the environment variables in a
  different way)

# Application server overview

In the server/src folder you will find the following files:

- mysql-pool.ts - contains the database configuration.
- task-service.ts - wraps the task related SQL calls in a class called `TaskService`. Exports both
  the Task type (interface) and an instance of the `TaskService` class.
- task-router.ts - creates the REST API `/tasks`.
- app.ts - sets up web server configuration.
- server.ts - starts web server on port 3000 (only used on `npm start`).

An example test is also added in the server/test folder:

- task-router.test.ts - uses the web server setup in ../src/app.ts to start another server running
  on port 3001, and tests the task REST API.

# Running application server

First you need to install the dependencies:

```sh
cd server
npm install
```

The server is using the following database table:

```sql
CREATE TABLE Tasks (
  id INT NOT NULL AUTO_INCREMENT,
  title TEXT NOT NULL,
  done BOOL DEFAULT false,
  PRIMARY KEY(id)
);
```

Create the above table on a MySQL database, for instance called `todo_dev`, running on your
localhost.

The server uses environment variables to setup the database connection (in
server/src/mysql-pool.ts).

You can set these environment variables when you start the server:

```sh
# In the server/ folder:
MYSQL_HOST=127.0.0.1 MYSQL_DATABASE=todo_dev MYSQL_USER=root npm start
```

The same environment variables must also be set when running the server tests:

```sh
# In the server/ folder:
MYSQL_HOST=127.0.0.1 MYSQL_DATABASE=todo_dev MYSQL_USER=root npm test
```

However, you should preferably use a different database for running tests, for instance `todo_test`.
